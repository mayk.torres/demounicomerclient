package com.unicomer.demounicomerclient.entities.controllers;

import com.unicomer.demounicomerclient.dto.MessageRs;
import com.unicomer.demounicomerclient.entities.Client;
import com.unicomer.demounicomerclient.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping("/save")
    public ResponseEntity<?> newClient ( @RequestBody Client client ){
        return new ResponseEntity(clienteService.saveClient(client), HttpStatus.OK);
    }

    @GetMapping("/getAllClientes")
    public ResponseEntity<List<Client>> allClients (){
        return new ResponseEntity(clienteService.getAllClients(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCliente (@PathVariable("id") Long id ){
        Client client = clienteService.getCliente(id);
        if (null!= client ){
            return new ResponseEntity(client, HttpStatus.OK);
        }else {
            return new ResponseEntity(new MessageRs("Cliente no encontrado"), HttpStatus.OK);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<?> update (@RequestBody Client client){
        return new ResponseEntity(clienteService.updateCliente(client), HttpStatus.OK);
    }

}
