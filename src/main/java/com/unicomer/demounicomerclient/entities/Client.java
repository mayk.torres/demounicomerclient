package com.unicomer.demounicomerclient.entities;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;




@Data
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idClient", nullable = false)
    private Long id;
    @Column(name = "firstName", nullable = false)
    private String firstName;
    @Column(name = "lastName", nullable = false)
    private String lastName;
    @Convert(disableConversion = true)
    @Column(name = "birthday", nullable = false)
    private LocalDate birthday;
    @Column(name = "gender", nullable = false)
    private String gender;
    @Column(name = "cellPhone", nullable = false)
    private Integer cellPhone;
    @Column(name = "homePhone", nullable = false)
    private Integer homePhone;
    @Column(name = "addressHome", nullable = false)
    private String addressHome;
    @Column(name = "profession", nullable = false)
    private String profession;
    @Column(name = "incomes", nullable = false)
    private BigDecimal incomes;




}
