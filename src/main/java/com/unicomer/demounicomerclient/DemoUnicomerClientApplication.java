package com.unicomer.demounicomerclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoUnicomerClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoUnicomerClientApplication.class, args);
    }

}
