package com.unicomer.demounicomerclient.repository;

import com.unicomer.demounicomerclient.entities.Client;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

}