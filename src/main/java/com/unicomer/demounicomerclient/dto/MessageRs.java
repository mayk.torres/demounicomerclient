package com.unicomer.demounicomerclient.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageRs {
private String message;
}
