package com.unicomer.demounicomerclient.service;

import com.unicomer.demounicomerclient.dto.MessageRs;
import com.unicomer.demounicomerclient.entities.Client;
import java.util.List;

public interface ClienteService {
    MessageRs saveClient (Client client);
    List<Client> getAllClients ();
    Client getCliente (Long idCliente);
    MessageRs updateCliente (Client client);

}
