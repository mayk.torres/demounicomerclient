package com.unicomer.demounicomerclient.service.impl;

import com.unicomer.demounicomerclient.dto.MessageRs;
import com.unicomer.demounicomerclient.entities.Client;
import com.unicomer.demounicomerclient.repository.ClientRepository;
import com.unicomer.demounicomerclient.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public MessageRs saveClient(Client client) {
        try {
            clientRepository.save(client);
        } catch (Exception e) {
            return new MessageRs("Error al guardar");
        }


        return new MessageRs("cliente guardado");
    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Client getCliente(Long idCliente) {
        Client client ;
        try {
             client = clientRepository.findById(idCliente).get();
        }catch (Exception e){
            return null;
        }
        return client;

    }

    @Override
    public MessageRs updateCliente(Client client) {
        try {
            Client clientBd = clientRepository.findById(client.getId()).get();
            clientBd.setFirstName((null != client.getFirstName()) ? client.getFirstName() : clientBd.getFirstName());
            clientBd.setLastName((null != client.getLastName()) ? client.getLastName() : clientBd.getLastName());
            clientBd.setBirthday((null != client.getBirthday()) ? client.getBirthday() : clientBd.getBirthday());
            clientBd.setGender((null != client.getGender()) ? client.getGender() : clientBd.getGender());
            clientBd.setCellPhone((null != client.getCellPhone()) ? client.getCellPhone() : clientBd.getCellPhone());
            clientBd.setHomePhone((null != client.getHomePhone()) ? client.getHomePhone() : clientBd.getHomePhone());
            clientBd.setAddressHome((null != client.getAddressHome()) ? client.getAddressHome() : clientBd.getAddressHome());
            clientBd.setProfession((null != client.getProfession()) ? client.getProfession() : clientBd.getProfession());
            clientBd.setIncomes((null != client.getIncomes()) ? client.getIncomes() : clientBd.getIncomes());
            clientRepository.save(clientBd);

        } catch (Exception e) {
            return new MessageRs("Error al Actualizar");
        }

        return new MessageRs("cliente actualizado");
    }
}
